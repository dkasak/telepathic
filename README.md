# Introduction

This is an interactive CLI client for [Brain.fm](https://brain.fm/).

# Building

In order to build this into a Python wheel, you need
[poetry](https://poetry.eustace.io/).

Clone this repository, change into its directory and run `poetry build`. This
will produce a Python wheel in `dist/`.

# Installation

Once you've built the wheel, you can install it using normal methods for Python
wheel installation, for example via `pip install`.

If you're on Arch Linux, there is a `PKGBUILD` available in
`contrib/arch_linux`. To produce (and install) an Arch Linux package, run

```
cd contrib/arch_linux
makepkg -is
```

The application depends on `mpv` for playing the stream, so you need to have
that installed too. You also need [Nerd Fonts](https://www.nerdfonts.com/) so
that the state icon (stopped, playing, paused) displays properly.

# Configuration

The configuration file is located at `$XDG_CONFIG_HOME/telepathic/config`. It's
in INI format. The first time you run telepathic, it will create this file and
populate it with all the settings available (but with the values left empty).

The only settings supported currently are your Brain.fm credentials:

```
[account]
email = my.email@here.com
password = hunter1
```

# Demonstration

[![telepathic demonstration](https://asciinema.org/a/27agkjIqXJD17Wu0yFmuSg1cJ)](https://asciinema.org/a/27agkjIqXJD17Wu0yFmuSg1cJ)
