"""The client class for interfacing with brain.fm"""

import json
import subprocess
import requests

import logging

from urllib.parse import quote

USER_AGENT = "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:70.0) Gecko/20100101 Firefox/70.0"


class TelepathicError(Exception):
    pass


class Client():
    SITE = "https://www.brain.fm"
    API = "https://api.brain.fm/v2"
    STREAMING_SITE = "https://audio.brain.fm"

    def __init__(self, email, password):
        global logger
        logger = logging.getLogger('telepathic')

        self.email = email
        self.password = password
        self.current_station = None

        self._prev_station_token = None
        self._station_token = None
        self._player = None

    def get(self, name):
        resp = requests.get(Client.api_endpoint(name), headers={"Authorization": f"Bearer {self.auth_token}"})

        if resp.status_code != 200:
            logger.error(f"Failed API call: <{resp.status_code}> {resp.url}")
            raise TelepathicError(f"Failed API call: {name}")

        return json.loads(resp.text)

    def post(self, name, data=None):
        if data is not None:
            resp = requests.post(Client.api_endpoint(name), headers={"Authorization": f"Bearer {self.auth_token}"}, json=data)
        else:
            resp = requests.post(Client.api_endpoint(name), headers={"Authorization": f"Bearer {self.auth_token}"})

        if resp.status_code != 200:
            logger.error(f"Failed API call: <{resp.status_code}> {resp.url}")
            raise TelepathicError(f"Failed API call: {name}")

        return json.loads(resp.text)

    @staticmethod
    def endpoint(name):
        return "{}{}".format(Client.SITE, name)

    @staticmethod
    def api_endpoint(name):
        return "{}{}".format(Client.API, name)

    @staticmethod
    def station_endpoint(url):
        name, params = url.split("?")
        return f"{Client.STREAMING_SITE}/{quote(name)}?{params}"

    def _get_user_id(self):
        return self.get("/users/me")["result"]["user"]["id"]

    def _get_stations(self):
        return self.post(f"/users/{self.user_id}/sessions", {"activityId":"6042a1bad80ae028b821e953","version":3})["result"]

    def _play(self, stream_url):
        if self._player:
            self._player.terminate()
            self._player.wait()

        if not self.auth_token:
            logger.error(
                f"Attempted to play a station, but not currently logged in.")
            raise TelepathicError("Not currently logged in.")

        play_stream = [
            "mpv",
            "--cookies",
            "--http-header-fields='User-Agent: {}','Authorization: Bearer {};'".format(
                USER_AGENT, self.auth_token),
            stream_url]

        logger.debug(f"Playing stream using command `{' '.join(play_stream)}`")

        self._player = subprocess.Popen(
            play_stream,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL)

    def login(self):
        data = {
            "email": self.email,
            "password": self.password,
        }

        logger.info("Logging in...")

        resp = requests.post(
            Client.api_endpoint("/auth/email-login"), json=data)

        if resp.status_code != 200:
            logger.error("Login failed.")
            return

        data = json.loads(resp.text)
        self.auth_token = data["result"]
        self.user_id = self._get_user_id()
        self.stations = self._get_stations()

        print(self.stations)

        logger.info("Logged in.")

    def station_names(self):
        return [s["name"] for s in self.stations]

    def find_station_by_name(self, station_name):
        for s in self.stations:
            if s["name"] == station_name:
                return s
        return None

    def select_station(self, station_name):
        if self._player:
            self.stop()

        self.prev_station = self.current_station

        station = self.find_station_by_name(station_name)
        if not station:
            print(f"No such station: {station_name}")
            return
        self.current_station = station

    def play(self, station_name):
        logger.info(f"Playing station: {station_name}")

        self.select_station(station_name)
        self._start()

        stream_url = Client.station_endpoint(self.current_station["url"])
        logger.debug(f"Stream URL: {stream_url}")

        self._play(stream_url)

    def start(self):
        if not self.current_station:
            logger.info(
                "Tried restarting the stream, "
                "but there is no station selected.")
            raise TelepathicError(
                "No station selected. Use `play` to select one.")

        logger.info(f"Restarting station {self.current_station['name']}")

        self._start()

    def _start(self):
        stream_url = Client.station_endpoint(self.current_station["url"])
        logger.debug(f"Stream URL: {stream_url}")

        self._play(stream_url)

    def stop(self):
        if not self.is_playing():
            logger.info(
                "Tried stopping the stream, but there is no station playing.")
            raise TelepathicError("No station is currently playing.")

        self._player.terminate()
        self._player.wait()
        logger.info(f"Stopped station: {self.current_station}")

    def next(self):
        self.stop()
        logger.info(f"Switching to the next track.")
        self._prev_station_token = self._station_token
        self.play(self.current_station)
        logger.debug(f"Previous station token: {self._prev_station_token}")
        logger.debug(f"New station token: {self._station_token}")

    def prev(self):
        if not self._prev_station_token:
            logger.info(
                "Tried switching to the previous track, "
                "but there was no previous track.")
            raise TelepathicError("No previous track was played.")

        logger.info("Switching to previous track.")
        self.stop()
        self._station_token = self._prev_station_token
        logger.debug(f"Previous station token: {self._prev_station_token}")
        self._prev_station_token = None
        self.start()

    def is_playing(self):
        return self._player and not self._player.returncode

    def is_paused(self):
        return self._station_token is not None and self._player.returncode
