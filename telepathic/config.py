import configparser
from os import path
import xdg.BaseDirectory as basedir


def ensure_config():
    basedir.save_config_path("telepathic")
    config_file = basedir.load_first_config("telepathic", "config")

    if not config_file:
        config_dir = basedir.load_first_config("telepathic")

        config = configparser.ConfigParser()
        config["account"] = {}
        config["account"]["email"] = ""
        config["account"]["password"] = ""

        with open(path.join(config_dir, "config"), "w") as f:
            config.write(f)


def get_config_path():
    return basedir.load_first_config("telepathic", "config")


def get_config():
    config_path = get_config_path()

    if not config_path:
        return None
    else:
        config = configparser.ConfigParser()
        config.read(config_path)

        return config

