"""The Brain.fm CLI client."""

import argparse
import asyncio
import atexit
import logging
import signal
import sys
import time
from typing import Dict, List

import attr
import click

from prompt_toolkit import HTML, PromptSession
from prompt_toolkit.completion import Completer, Completion
from prompt_toolkit.patch_stdout import patch_stdout
from prompt_toolkit.styles import Style

from telepathic.client import Client, TelepathicError
from telepathic.config import ensure_config, get_config, get_config_path

style = Style.from_dict({
    'station': '#d35c5c bold',
    'sigil': '#e0ac16',
    'bottom-toolbar': 'noreverse',
})


def quoted(xs):
    return [f'"{x}"' for x in xs]

class ParseError(Exception):
    pass


class CLIArgParse(argparse.ArgumentParser):
    def print_usage(self, file=None):
        pass

    def error(self, message):
        message = f"Error: {message} " f"(see help)"
        print(message)
        raise ParseError


class CLIParser:
    def __init__(self, commands):
        self.commands = commands
        self.parser = CLIArgParse()

        subparsers = self.parser.add_subparsers(dest="subcommand")

        help_ = subparsers.add_parser("help")
        help_.add_argument("command", nargs='?', choices=self.commands)

        list_ = subparsers.add_parser("list")

        play = subparsers.add_parser("play")
        play.add_argument("station", type=str)

        select = subparsers.add_parser("select")
        select.add_argument("station", type=str)

        stop = subparsers.add_parser("stop")
        start = subparsers.add_parser("start")
        next_ = subparsers.add_parser("next")
        prev = subparsers.add_parser("prev")

    def parse_args(self, argv):
        tokens = self.split_quoted(argv)
        return self.parser.parse_args(tokens)

    def split_quoted(self, s):
        quoted = False
        tokens = []

        while True:
            i = s.find('"')

            if i == -1:
                tokens.extend(s.strip().split())
                break

            if quoted:
                tokens.append(s[:i])
            else:
                tokens.extend(s[:i].split())

            quoted = not quoted
            s = s[i+1:]

        if quoted:
            self.parser.error("Unclosed quote.")  # raises ParseError

        return tokens


@attr.s
class CLI():
    command_help = {
        "select": "Select station.",
        "play": "Select station and start playing it immediately (see `list`).",
        "list": "List available stations.",
        "stop": "Stop the currently playing track.",
        "start": "Resume playing the stopped track (if any).",
        "next": "Switch to the next track on the same station.",
        "prev": "Switch to the prev track on the same station.",
        "help": "Display help.",
    }

    client = attr.ib(type=Client)

    completer = attr.ib(init=False)
    commands = list(command_help.keys())

    def __attrs_post_init__(self):
        self.client.login()
        self.completer = CLICompleter(
            self.command_help, quoted(self.client.station_names()))

    def show_help(self, command):
        if command:
            print(self.command_help[command])
        else:
            formatted_help = "\n".join(f"{command}\t{command_help}" for command,
                                       command_help in self.command_help.items())
            formatted_help = f"Commands:\n{formatted_help}\n"
            print(formatted_help)

    @staticmethod
    def bottom_toolbar(client):
        def inner():
            if not client.current_station:
                return HTML(f'No station selected.')
            else:
                if client.is_paused():
                    sigil = ""
                elif client.is_playing():
                    sigil = ""
                else:
                    sigil = ""
                return HTML(f'<sigil>{sigil}</sigil> Station: <station>{client.current_station["name"]}</station>')

        return inner

    async def loop(self):
        """CLI event loop"""
        promptsession = PromptSession("telepathic> ",
                                      completer=self.completer,
                                      bottom_toolbar=CLI.bottom_toolbar(
                                          self.client),
                                      style=style)

        while True:
            with patch_stdout():
                try:
                    result = await promptsession.prompt_async()
                except EOFError:
                    break

            if not result:
                continue

            try:
                self.execute_command(result)
            except ParseError:
                continue

    def execute_command(self, command):
        """Execute a command string (e.g. "play STATION")."""
        parser = CLIParser(self.commands)
        args = parser.parse_args(command)  # may raise ParseError
        command = args.subcommand

        try:
            if command == "play":
                self.client.play(args.station)
            elif command == "select":
                self.client.select_station(args.station)
            elif command == "list":
                print("\n".join(quoted(self.client.station_names())))
            elif command == "start":
                self.client.start()
            elif command == "stop":
                self.client.stop()
            elif command == "next":
                self.client.next()
            elif command == "prev":
                self.client.prev()
            elif command == "help":
                self.show_help(args.command)
        except TelepathicError as e:
            print(e)


@attr.s
class CLICompleter(Completer):
    """Completer for CLI commands."""

    command_help = attr.ib(type=Dict[str, str])
    stations = attr.ib(type=List[str])

    def __attrs_post_init__(self):
        self.commands = list(self.command_help.keys())

    def complete_commands(self, last_word):
        """Complete the available commands."""
        completions = self.filter_substrings(self.commands, last_word)
        yield from [Completion(c, -len(last_word)) for c in completions]

    def complete_stations(self, last_word):
        """Complete the supported Brain.fm stations."""
        completions = self.filter_substrings(self.stations, last_word)
        yield from [Completion(c, -len(last_word)) for c in completions]

    @staticmethod
    def filter_substrings(strings, substring):
        return [s for s in strings if substring in s]

    def get_completions(self, document, complete_event):
        """Build the completions."""
        text_before_cursor = str(document.text_before_cursor)
        words = text_before_cursor.split(" ")

        last_word = words[-1]

        if len(words) == 1:
            return self.complete_commands(last_word)

        if len(words) > 1:
            command = words[0]

            if command in ["play", "select"]:
                if len(words) == 2:
                    return self.complete_stations(last_word)
                else:
                    return ""
            elif command == "help":
                if len(words) == 2:
                    return self.complete_commands(last_word)
                else:
                    return ""

        return ""


@click.command(
    help=(
        """telepathic is an interactive CLI Brain.fm client.

        If STATION is passed, it will be played without starting the TUI.
        """
    )
)
@click.version_option(version="0.1", prog_name="telepathic")
@click.option('-d', '--debug', count=True,
              help="Increase the log level. Can be passed multiple times.")
@click.argument('station', required=False)
def main(debug, station):
    global logger
    logger = logging.getLogger('telepathic')
    formatter = logging.Formatter(
        '[%(asctime)s] %(name)s (%(levelname)s): %(message)s')
    stdout_handler = logging.StreamHandler()
    stdout_handler.setFormatter(formatter)
    logger.addHandler(stdout_handler)

    if debug == 0:
        logger.setLevel(logging.WARN)
    elif debug == 1:
        logger.setLevel(logging.INFO)
    elif debug >= 2:
        logger.setLevel(logging.DEBUG)

    loop = asyncio.get_event_loop()

    ensure_config()
    config = get_config()
    email = config["account"]["email"]
    password = config["account"]["password"]

    if not email or not password:
        config_path = get_config_path()
        logger.error("Email or password not configured.")
        print(f"Please configure your email and password in {config_path}.")
        sys.exit(1)

    client = Client(email, password)
    logger.info("Instantiated client.")

    cli = CLI(client)
    logger.info("Initialized CLI.")

    def stop_at_end():
        try:
            client.stop()
        except TelepathicError:
            pass

    def sigterm_handler(_signum, _frame):
        stop_at_end()
        sys.exit(0)

    atexit.register(stop_at_end)
    signal.signal(signal.SIGTERM, sigterm_handler)

    if station:
        try:
            client.play(station)

            while True:
                time.sleep(600)
        except ParseError:
            # No need to print anything since the command parser will already
            # output a nice error message.
            pass
    else:
        try:
            loop.run_until_complete(cli.loop())
        except KeyboardInterrupt:
            pass


if __name__ == '__main__':
    main()
