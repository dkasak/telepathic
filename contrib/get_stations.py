# requires jq

import sys
import requests
from subprocess import run

from telepathic.client import Client
from telepathic.config import get_config, get_config_path

JQ_PARSE_DATA = '''
[ .stations
| .[]
| select(.string_id != null)
| {(.string_id): .id}] | add
'''


def main():
    config = get_config()

    if not config:
        config_path = get_config_path()
        print(f"You don't have a config file at {config_path}. "
               "Cannot retrieve credentials.")
        sys.exit(1)

    email = config["account"]["email"]
    password = config["account"]["password"]

    if not email or not password:
        print(f"Please configure your email and password in {config_path}.")
        sys.exit(1)

    data = {
        "type": "LOGIN",
        "email": email,
        "password": password,
    }

    session_token = requests.post(
        Client.endpoint("login"), json=data).cookies['connect.sid']
    auth_cookie = {'connect.sid': session_token}
    response = requests.get(Client.endpoint("app/player"),
                            cookies=auth_cookie).content.decode('utf-8')

    for line in response.splitlines():
        if "PRELOADED_STATE" in line:
            line = line.rstrip(';')
            station_data = line.split('=', 1)[1]

    print(run(["jq", JQ_PARSE_DATA], text=True, input=station_data))


if __name__ == '__main__':
    main()
